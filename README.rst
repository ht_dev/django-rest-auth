Welcome to django-rest-auth (Edited version by Jhovel)
===========================

.. image:: https://travis-ci.org/Tivix/django-rest-auth.svg
    :target: https://travis-ci.org/Tivix/django-rest-auth


.. image:: https://coveralls.io/repos/Tivix/django-rest-auth/badge.svg
    :target: https://coveralls.io/r/Tivix/django-rest-auth?branch=master


.. image:: https://readthedocs.org/projects/django-rest-auth/badge/?version=latest
    :target: https://readthedocs.org/projects/django-rest-auth/?badge=latest


Django-rest-auth provides a set of REST API endpoints for Authentication and Registration


Documentation
-------------

This version does support SimpleJWT Login, but rest of the methods are untouched.
http://django-rest-auth.readthedocs.org/en/latest/


Source code
-----------
https://github.com/Tivix/django-rest-auth


Stack Overflow
-----------
http://stackoverflow.com/questions/tagged/django-rest-auth


#### Installation for this Repo:

```
pip install git+https://jhovel@bitbucket.org/ht_dev/django-rest-auth.git

Install Simple JWT https://github.com/davesque/django-rest-framework-simplejwt
- pip install djangorestframework_simplejwt
- add this on installed apps
- 'rest_framework_simplejwt.authentication.JWTAuthentication'
- if you want to use JWT blocklist functionality include this,
- 'rest_framework_simplejwt.token_blacklist'
- ./manage.py migrate

After installing JWT 
- set REST_USE_JWT = True
 

```

